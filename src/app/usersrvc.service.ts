import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersrvcService {
  url = 'http://49.12.217.12:5000';
  remember: boolean = true;
  constructor(private http: HttpClient) { }



  login(body: { name: "", email: "", password: "", remember: "" }) {
    const headers = {};
    return this.http.post(this.url + '/login_angular', body, { headers })
    // ).subscribe(res => {
    //   console.log("res", res);
    // localStorage.setItem('userInfo', res.id);

  }

  signup(body: any) {
    const headers = {};
    return this.http.post(this.url + '/signup_angular', body, { headers });
  }

  // getUserData(body: any) {
  //   const headers = {};
  //   this.http.post<any>(this.url + '/login_angular', body, { headers })

  // }

  getProfileData() {
    const headers = {};
    const id = localStorage.getItem('id');
    const body = { id: id };

    return this.http.post<any>(this.url + '/get_profile_angular', body, { headers }
    )
  }
}
