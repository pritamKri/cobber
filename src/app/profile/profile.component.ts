import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersrvcService } from '../usersrvc.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(private usersvc: UsersrvcService, private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute) {
  }
  
  userData: any;

  ngOnInit() {
    this.profileData();
  }

  profileData() {
    this.usersvc.getProfileData().subscribe((data: any) => {
      console.log("profile", data.email)
      this.userData = data;
    })
  }


}






