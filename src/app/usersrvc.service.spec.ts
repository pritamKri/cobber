import { TestBed } from '@angular/core/testing';

import { UsersrvcService } from './usersrvc.service';

describe('UsersrvcService', () => {
  let service: UsersrvcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsersrvcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
